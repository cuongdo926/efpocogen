﻿using System;
using System.Windows.Forms;
using EfPocoGen.Core.Infrastructure;
using EfPocoGen.WinApp.Helper;
using Microsoft.Data.ConnectionUI;

namespace EfPocoGen.WinApp
{
    public partial class FrmConnection : Form
    {
        private const string DefaultConnString = "Data Source=.\\SQLExpress;Initial Catalog=Sample;Integrated Security=SSPI;";

        private Connection _connection;
        public Connection Connection
        {
            get { return _connection; }
            set
            {
                _connection = value;
                BindData();
            }
        }

        public FrmConnection()
        {
            InitializeComponent();
            Load += FrmConnection_Load;
        }

        private void FrmConnection_Load(object sender, EventArgs e)
        {
            if (Connection == null)
            {
                Connection = CreateNewConnection();
            }
        }

        private Connection CreateNewConnection()
        {
            return new Connection
            {
                Id = Guid.NewGuid(),
                Name = "New Connection",
                ConnectionString = DefaultConnString
            };
        }

        private void BindData()
        {
            txtName.Text = Connection.Name;
            txtConnString.Text = Connection.ConnectionString;
        }

        private void CaptureConnection()
        {
            Connection.Name = txtName.Text;
            Connection.ConnectionString = txtConnString.Text;
        }

        private void btnConnString_Click(object sender, EventArgs e)
        {
            var dialogResult = DialogResult.Cancel;
            var connectionString = string.Empty;

            var dcd = new DataConnectionDialog();

            try
            {
                var dcs = new DataConnectionConfiguration(null);
                dcs.LoadConfiguration(dcd);

                CaptureConnection();
                if (Connection.ConnectionString != DefaultConnString)
                {
                    dcd.ConnectionString = Connection.ConnectionString;
                }

                dialogResult = DataConnectionDialog.Show(dcd);
                connectionString = dcd.ConnectionString;

            }
            catch (ArgumentException)
            {
                dcd.ConnectionString = string.Empty;
                dialogResult = DataConnectionDialog.Show(dcd);
            }
            finally
            {
                if (dialogResult == DialogResult.OK)
                {
                    Connection.ConnectionString = connectionString;
                    BindData();
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            CaptureConnection();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Connection = CreateNewConnection();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Abort;
        }

    }
}
