﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using EfPocoGen.Core.Generator;
using EfPocoGen.Core.Infrastructure;
using EfPocoGen.WinApp.Properties;
using FastColoredTextBoxNS;

namespace EfPocoGen.WinApp
{
    public partial class FrmMain : Form
    {
        private AppRef _appRef;
        private FileManager _fileManager;
        private Connection _currentConnection;

        public Connection CurrentConnection
        {
            get { return _currentConnection; }
            set
            {
                _currentConnection = value;
                btnConnect.Enabled = _currentConnection != null;
            }
        }

        public Table CurrentTable
        {
            get { return lstTable.SelectedValue as Table; }
        }

        private readonly ConnectionManager _connectionManager = new ConnectionManager();

        public FrmMain()
        {
            InitializeComponent();
        }

        private void RestoreSettings()
        {
            _connectionManager.Load(Settings.Default.Connections);
            RestoreAppSetting();
            BindConnections();

            if (!string.IsNullOrWhiteSpace(Settings.Default.LastConnectionId))
            {
                cbConnection.SelectedValue = Guid.Parse(Settings.Default.LastConnectionId);
            }
        }

        private void RestoreAppSetting()
        {
            var appSetting = AppSetting.Load(Settings.Default.AppSetting);
            if (appSetting == null) return;

            foreach (Control control in tabSettings.Controls)
            {
                BindSettingControls(control, appSetting);
            }
        }

        private void BindConnections()
        {
            cbConnection.DataSource = null;
            cbConnection.DataSource = _connectionManager.Connections;
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (CurrentConnection == null) return;

            try
            {
                _appRef = new AppRef
                {
                    ConnectionString = CurrentConnection.ConnectionString,
                };

                _fileManager = new FileManager(_appRef);
                _appRef.LoadTables(_appRef.GetDbProviderFactory());

                BindAppSetting();

                BindTables();
                gbFunctions.Enabled = true;
            }
            catch (Exception ex)
            {
                gbFunctions.Enabled = false;
                MessageBox.Show(ex.Message);
            }
        }

        private void BindAppSetting()
        {
            foreach (Control control in tabSettings.Controls)
            {
                SetAppRefSettingValues(control);
            }
        }

        private void BindTables()
        {
            lstTable.DataSource = _appRef.AllTables.OrderBy(x => x.Name).ToList();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            RestoreSettings();
            SetOutputFolder();
        }

        private void SetOutputFolder()
        {
            if (Directory.Exists(AppRef.Output))
            {
                lblOutputFolder.Text = AppRef.Output;
            }
        }

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveSettings();
        }

        private void SaveSettings()
        {
            Settings.Default.LastConnectionId = CurrentConnection != null
                ? CurrentConnection.Id.ToString() : string.Empty;
            Settings.Default.Connections = _connectionManager.ToJsonString();
            if (_appRef != null)
            {
                Settings.Default.AppSetting = _appRef.AppSetting.ToJsonString();
            }

            Settings.Default.Save();
        }

        private void btnGenAll_Click(object sender, EventArgs e)
        {
            GenerateAll<EntityGenerator>();
            GenerateAll<EntityMapGenerator>();
            GenerateAll<ModelGenerator>();
            Generate<DbContextGenerator>(null, writeToFile: true);
            ShowComplete();
        }

        private void ShowComplete()
        {
            SetOutputFolder();
            MessageBox.Show("Done!");
        }

        private void Generate<T>(Table table, FastColoredTextBox textBox = null, bool writeToFile = false)
            where T : AbstractGenerator
        {
            using (var gen = Activator.CreateInstance<T>())
            {
                gen.AppRefs = _appRef;
                gen.Table = table;
                gen.Generate();
                if (textBox != null)
                {
                    textBox.Text = gen.ToString();
                }
                if (writeToFile)
                {
                    _fileManager.WriteToFile(gen);
                }
            }
        }

        private void GenerateAll<T>() where T : AbstractGenerator
        {
            foreach (var table in GetAllTables())
            {
                Generate<T>(table, writeToFile: true);
            }
        }

        private IEnumerable<Table> GetAllTables()
        {
            return _appRef.AllTables.Where(x => !x.IsMapping);
        }

        private void btnGenContext_Click(object sender, EventArgs e)
        {
            Generate<DbContextGenerator>(null, writeToFile: true);
            ShowComplete();
        }

        private void btnGenModel_Click(object sender, EventArgs e)
        {
            GenerateAll<ModelGenerator>();
            ShowComplete();
        }


        private void btnService_Click(object sender, EventArgs e)
        {
            GenerateAll<ServiceGenerator>();
            ShowComplete();
        }

        private void btnGenEntity_Click(object sender, EventArgs e)
        {
            GenerateAll<EntityGenerator>();
            ShowComplete();
        }

        private void btnGenEntityMap_Click(object sender, EventArgs e)
        {
            GenerateAll<EntityMapGenerator>();
            ShowComplete();
        }

        private void btnConnString_Click(object sender, EventArgs e)
        {
            var dlg = new FrmConnection();

            if (CurrentConnection != null)
            {
                dlg.Connection = CurrentConnection;
            }

            var result = dlg.ShowDialog(this);
            switch (result)
            {
                case DialogResult.OK:
                    if (CurrentConnection == null ||
                        CurrentConnection.Id != dlg.Connection.Id)
                    {
                        _connectionManager.Add(dlg.Connection);
                    }
                    BindConnections();
                    break;
                case DialogResult.Abort:
                    _connectionManager.Remove(CurrentConnection);
                    CurrentConnection = null;
                    BindConnections();
                    break;
            }
        }

        private void cbConnection_SelectedIndexChanged(object sender, EventArgs e)
        {
            CurrentConnection = cbConnection.SelectedItem as Connection;
        }

        private void txtSettings_TextChanged(object sender, EventArgs e)
        {
            SetAppRefSettingValues(sender as Control);
        }

        private void lstTable_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CurrentTable == null) return;

            Generate<EntityGenerator>(CurrentTable, txtEntityGenerator);
            Generate<EntityMapGenerator>(CurrentTable, txtMapGenerator);
            Generate<ModelGenerator>(CurrentTable, txtModelGenerator);
            Generate<ServiceGenerator>(CurrentTable, txtRepoGenerator);
            Generate<DbContextGenerator>(CurrentTable, txtContextGenerator);
        }

        private void SetAppRefSettingValues(Control control)
        {
            var name = control.Name;
            if (!name.StartsWith("txt") || _appRef == null) return;

            var field = _appRef.AppSetting.GetType().GetField(name.Substring(3));
            if (field == null) return;

            field.SetValue(_appRef.AppSetting, control.Text);
        }
        private void BindSettingControls(Control control, AppSetting appSetting)
        {
            var name = control.Name;
            if (!name.StartsWith("txt") || appSetting == null) return;

            var field = appSetting.GetType().GetField(name.Substring(3));
            if (field == null) return;
            control.Text = field.GetValue(appSetting).ToString();
        }

        private void lblOutputFolder_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (Directory.Exists(lblOutputFolder.Text))
            {
                Process.Start(lblOutputFolder.Text);
            }
        }

    }
}
