﻿namespace EfPocoGen.WinApp
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnGenAll = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnGenModel = new System.Windows.Forms.Button();
            this.btnGenEntity = new System.Windows.Forms.Button();
            this.tabParent = new System.Windows.Forms.TabControl();
            this.tabBasic = new System.Windows.Forms.TabPage();
            this.tabOutput = new System.Windows.Forms.TabControl();
            this.tabEntity = new System.Windows.Forms.TabPage();
            this.txtEntityGenerator = new FastColoredTextBoxNS.FastColoredTextBox();
            this.tabMap = new System.Windows.Forms.TabPage();
            this.txtMapGenerator = new FastColoredTextBoxNS.FastColoredTextBox();
            this.tabRepo = new System.Windows.Forms.TabPage();
            this.txtRepoGenerator = new FastColoredTextBoxNS.FastColoredTextBox();
            this.tabModel = new System.Windows.Forms.TabPage();
            this.txtModelGenerator = new FastColoredTextBoxNS.FastColoredTextBox();
            this.tabContext = new System.Windows.Forms.TabPage();
            this.txtContextGenerator = new FastColoredTextBoxNS.FastColoredTextBox();
            this.gbFunctions = new System.Windows.Forms.GroupBox();
            this.lblOutputFolder = new System.Windows.Forms.LinkLabel();
            this.btnService = new System.Windows.Forms.Button();
            this.btnGenContext = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.btnGenEntityMap = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lstTable = new System.Windows.Forms.ListBox();
            this.btnConnString = new System.Windows.Forms.Button();
            this.cbConnection = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabSettings = new System.Windows.Forms.TabPage();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtContextName = new System.Windows.Forms.TextBox();
            this.txtDomainContextNs = new System.Windows.Forms.TextBox();
            this.txtDomainModelNs = new System.Windows.Forms.TextBox();
            this.txtDomainRepoNs = new System.Windows.Forms.TextBox();
            this.txtDomainConfigurationNs = new System.Windows.Forms.TextBox();
            this.txtDomainEntityNs = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCoreAutoMapperNs = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtCoreContextNs = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCoreRepoNs = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCoreUowNs = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCoreEntityNs = new System.Windows.Forms.TextBox();
            this.tabParent.SuspendLayout();
            this.tabBasic.SuspendLayout();
            this.tabOutput.SuspendLayout();
            this.tabEntity.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEntityGenerator)).BeginInit();
            this.tabMap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMapGenerator)).BeginInit();
            this.tabRepo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtRepoGenerator)).BeginInit();
            this.tabModel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtModelGenerator)).BeginInit();
            this.tabContext.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtContextGenerator)).BeginInit();
            this.gbFunctions.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnGenAll
            // 
            this.btnGenAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenAll.Location = new System.Drawing.Point(413, 42);
            this.btnGenAll.Name = "btnGenAll";
            this.btnGenAll.Size = new System.Drawing.Size(75, 23);
            this.btnGenAll.TabIndex = 1;
            this.btnGenAll.Text = "Everything";
            this.btnGenAll.UseVisualStyleBackColor = true;
            this.btnGenAll.Click += new System.EventHandler(this.btnGenAll_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.Enabled = false;
            this.btnConnect.Location = new System.Drawing.Point(215, 21);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 2;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnGenModel
            // 
            this.btnGenModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenModel.Location = new System.Drawing.Point(332, 42);
            this.btnGenModel.Name = "btnGenModel";
            this.btnGenModel.Size = new System.Drawing.Size(75, 23);
            this.btnGenModel.TabIndex = 5;
            this.btnGenModel.Text = "Model";
            this.btnGenModel.UseVisualStyleBackColor = true;
            this.btnGenModel.Click += new System.EventHandler(this.btnGenModel_Click);
            // 
            // btnGenEntity
            // 
            this.btnGenEntity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenEntity.Location = new System.Drawing.Point(8, 42);
            this.btnGenEntity.Name = "btnGenEntity";
            this.btnGenEntity.Size = new System.Drawing.Size(75, 23);
            this.btnGenEntity.TabIndex = 5;
            this.btnGenEntity.Text = "Entity";
            this.btnGenEntity.UseVisualStyleBackColor = true;
            this.btnGenEntity.Click += new System.EventHandler(this.btnGenEntity_Click);
            // 
            // tabParent
            // 
            this.tabParent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabParent.Controls.Add(this.tabBasic);
            this.tabParent.Controls.Add(this.tabSettings);
            this.tabParent.Location = new System.Drawing.Point(7, 4);
            this.tabParent.Name = "tabParent";
            this.tabParent.SelectedIndex = 0;
            this.tabParent.Size = new System.Drawing.Size(1045, 615);
            this.tabParent.TabIndex = 7;
            // 
            // tabBasic
            // 
            this.tabBasic.Controls.Add(this.tabOutput);
            this.tabBasic.Controls.Add(this.gbFunctions);
            this.tabBasic.Controls.Add(this.groupBox1);
            this.tabBasic.Controls.Add(this.btnConnString);
            this.tabBasic.Controls.Add(this.cbConnection);
            this.tabBasic.Controls.Add(this.label1);
            this.tabBasic.Controls.Add(this.btnConnect);
            this.tabBasic.Location = new System.Drawing.Point(4, 22);
            this.tabBasic.Name = "tabBasic";
            this.tabBasic.Padding = new System.Windows.Forms.Padding(3);
            this.tabBasic.Size = new System.Drawing.Size(1037, 589);
            this.tabBasic.TabIndex = 0;
            this.tabBasic.Text = "Basic";
            this.tabBasic.UseVisualStyleBackColor = true;
            // 
            // tabOutput
            // 
            this.tabOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabOutput.Controls.Add(this.tabEntity);
            this.tabOutput.Controls.Add(this.tabMap);
            this.tabOutput.Controls.Add(this.tabRepo);
            this.tabOutput.Controls.Add(this.tabModel);
            this.tabOutput.Controls.Add(this.tabContext);
            this.tabOutput.Location = new System.Drawing.Point(255, 131);
            this.tabOutput.Name = "tabOutput";
            this.tabOutput.SelectedIndex = 0;
            this.tabOutput.Size = new System.Drawing.Size(776, 452);
            this.tabOutput.TabIndex = 23;
            // 
            // tabEntity
            // 
            this.tabEntity.Controls.Add(this.txtEntityGenerator);
            this.tabEntity.Location = new System.Drawing.Point(4, 22);
            this.tabEntity.Name = "tabEntity";
            this.tabEntity.Padding = new System.Windows.Forms.Padding(3);
            this.tabEntity.Size = new System.Drawing.Size(768, 426);
            this.tabEntity.TabIndex = 11;
            this.tabEntity.Text = "Entity";
            this.tabEntity.UseVisualStyleBackColor = true;
            // 
            // txtEntityGenerator
            // 
            this.txtEntityGenerator.AutoScrollMinSize = new System.Drawing.Size(27, 14);
            this.txtEntityGenerator.BackBrush = null;
            this.txtEntityGenerator.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtEntityGenerator.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtEntityGenerator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEntityGenerator.Font = new System.Drawing.Font("Courier New", 9.75F);
            this.txtEntityGenerator.IsReplaceMode = false;
            this.txtEntityGenerator.Language = FastColoredTextBoxNS.Language.CSharp;
            this.txtEntityGenerator.LeftBracket = '(';
            this.txtEntityGenerator.Location = new System.Drawing.Point(3, 3);
            this.txtEntityGenerator.Name = "txtEntityGenerator";
            this.txtEntityGenerator.Paddings = new System.Windows.Forms.Padding(0);
            this.txtEntityGenerator.RightBracket = ')';
            this.txtEntityGenerator.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.txtEntityGenerator.Size = new System.Drawing.Size(762, 420);
            this.txtEntityGenerator.TabIndex = 4;
            // 
            // tabMap
            // 
            this.tabMap.Controls.Add(this.txtMapGenerator);
            this.tabMap.Location = new System.Drawing.Point(4, 22);
            this.tabMap.Name = "tabMap";
            this.tabMap.Padding = new System.Windows.Forms.Padding(3);
            this.tabMap.Size = new System.Drawing.Size(768, 426);
            this.tabMap.TabIndex = 12;
            this.tabMap.Text = "Map";
            this.tabMap.UseVisualStyleBackColor = true;
            // 
            // txtMapGenerator
            // 
            this.txtMapGenerator.AutoScrollMinSize = new System.Drawing.Size(27, 14);
            this.txtMapGenerator.BackBrush = null;
            this.txtMapGenerator.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtMapGenerator.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtMapGenerator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMapGenerator.Font = new System.Drawing.Font("Courier New", 9.75F);
            this.txtMapGenerator.IsReplaceMode = false;
            this.txtMapGenerator.Language = FastColoredTextBoxNS.Language.CSharp;
            this.txtMapGenerator.LeftBracket = '(';
            this.txtMapGenerator.Location = new System.Drawing.Point(3, 3);
            this.txtMapGenerator.Name = "txtMapGenerator";
            this.txtMapGenerator.Paddings = new System.Windows.Forms.Padding(0);
            this.txtMapGenerator.RightBracket = ')';
            this.txtMapGenerator.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.txtMapGenerator.Size = new System.Drawing.Size(762, 420);
            this.txtMapGenerator.TabIndex = 5;
            // 
            // tabRepo
            // 
            this.tabRepo.Controls.Add(this.txtRepoGenerator);
            this.tabRepo.Location = new System.Drawing.Point(4, 22);
            this.tabRepo.Name = "tabRepo";
            this.tabRepo.Padding = new System.Windows.Forms.Padding(3);
            this.tabRepo.Size = new System.Drawing.Size(768, 426);
            this.tabRepo.TabIndex = 13;
            this.tabRepo.Text = "Service";
            this.tabRepo.UseVisualStyleBackColor = true;
            // 
            // txtRepoGenerator
            // 
            this.txtRepoGenerator.AutoScrollMinSize = new System.Drawing.Size(27, 14);
            this.txtRepoGenerator.BackBrush = null;
            this.txtRepoGenerator.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtRepoGenerator.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtRepoGenerator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRepoGenerator.IsReplaceMode = false;
            this.txtRepoGenerator.Language = FastColoredTextBoxNS.Language.CSharp;
            this.txtRepoGenerator.LeftBracket = '(';
            this.txtRepoGenerator.Location = new System.Drawing.Point(3, 3);
            this.txtRepoGenerator.Name = "txtRepoGenerator";
            this.txtRepoGenerator.Paddings = new System.Windows.Forms.Padding(0);
            this.txtRepoGenerator.RightBracket = ')';
            this.txtRepoGenerator.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.txtRepoGenerator.Size = new System.Drawing.Size(762, 420);
            this.txtRepoGenerator.TabIndex = 5;
            // 
            // tabModel
            // 
            this.tabModel.Controls.Add(this.txtModelGenerator);
            this.tabModel.Location = new System.Drawing.Point(4, 22);
            this.tabModel.Name = "tabModel";
            this.tabModel.Padding = new System.Windows.Forms.Padding(3);
            this.tabModel.Size = new System.Drawing.Size(768, 426);
            this.tabModel.TabIndex = 14;
            this.tabModel.Text = "Model";
            this.tabModel.UseVisualStyleBackColor = true;
            // 
            // txtModelGenerator
            // 
            this.txtModelGenerator.AutoScrollMinSize = new System.Drawing.Size(27, 14);
            this.txtModelGenerator.BackBrush = null;
            this.txtModelGenerator.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtModelGenerator.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtModelGenerator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtModelGenerator.IsReplaceMode = false;
            this.txtModelGenerator.Language = FastColoredTextBoxNS.Language.CSharp;
            this.txtModelGenerator.LeftBracket = '(';
            this.txtModelGenerator.Location = new System.Drawing.Point(3, 3);
            this.txtModelGenerator.Name = "txtModelGenerator";
            this.txtModelGenerator.Paddings = new System.Windows.Forms.Padding(0);
            this.txtModelGenerator.RightBracket = ')';
            this.txtModelGenerator.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.txtModelGenerator.Size = new System.Drawing.Size(762, 420);
            this.txtModelGenerator.TabIndex = 6;
            // 
            // tabContext
            // 
            this.tabContext.Controls.Add(this.txtContextGenerator);
            this.tabContext.Location = new System.Drawing.Point(4, 22);
            this.tabContext.Name = "tabContext";
            this.tabContext.Padding = new System.Windows.Forms.Padding(3);
            this.tabContext.Size = new System.Drawing.Size(768, 426);
            this.tabContext.TabIndex = 15;
            this.tabContext.Text = "Context";
            this.tabContext.UseVisualStyleBackColor = true;
            // 
            // txtContextGenerator
            // 
            this.txtContextGenerator.AutoScrollMinSize = new System.Drawing.Size(27, 14);
            this.txtContextGenerator.BackBrush = null;
            this.txtContextGenerator.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtContextGenerator.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtContextGenerator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtContextGenerator.Font = new System.Drawing.Font("Courier New", 9.75F);
            this.txtContextGenerator.IsReplaceMode = false;
            this.txtContextGenerator.Language = FastColoredTextBoxNS.Language.CSharp;
            this.txtContextGenerator.LeftBracket = '(';
            this.txtContextGenerator.Location = new System.Drawing.Point(3, 3);
            this.txtContextGenerator.Name = "txtContextGenerator";
            this.txtContextGenerator.Paddings = new System.Windows.Forms.Padding(0);
            this.txtContextGenerator.RightBracket = ')';
            this.txtContextGenerator.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.txtContextGenerator.Size = new System.Drawing.Size(762, 420);
            this.txtContextGenerator.TabIndex = 7;
            // 
            // gbFunctions
            // 
            this.gbFunctions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbFunctions.Controls.Add(this.lblOutputFolder);
            this.gbFunctions.Controls.Add(this.btnService);
            this.gbFunctions.Controls.Add(this.btnGenContext);
            this.gbFunctions.Controls.Add(this.label13);
            this.gbFunctions.Controls.Add(this.btnGenEntity);
            this.gbFunctions.Controls.Add(this.btnGenModel);
            this.gbFunctions.Controls.Add(this.btnGenEntityMap);
            this.gbFunctions.Controls.Add(this.btnGenAll);
            this.gbFunctions.Enabled = false;
            this.gbFunctions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbFunctions.Location = new System.Drawing.Point(9, 52);
            this.gbFunctions.Name = "gbFunctions";
            this.gbFunctions.Size = new System.Drawing.Size(1022, 73);
            this.gbFunctions.TabIndex = 11;
            this.gbFunctions.TabStop = false;
            this.gbFunctions.Text = "Generate All";
            // 
            // lblOutputFolder
            // 
            this.lblOutputFolder.AutoSize = true;
            this.lblOutputFolder.Location = new System.Drawing.Point(86, 20);
            this.lblOutputFolder.Name = "lblOutputFolder";
            this.lblOutputFolder.Size = new System.Drawing.Size(53, 13);
            this.lblOutputFolder.TabIndex = 7;
            this.lblOutputFolder.TabStop = true;
            this.lblOutputFolder.Text = "[Output]";
            this.lblOutputFolder.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblOutputFolder_LinkClicked);
            // 
            // btnService
            // 
            this.btnService.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnService.Location = new System.Drawing.Point(170, 42);
            this.btnService.Name = "btnService";
            this.btnService.Size = new System.Drawing.Size(75, 23);
            this.btnService.TabIndex = 5;
            this.btnService.Text = "Service";
            this.btnService.UseVisualStyleBackColor = true;
            this.btnService.Click += new System.EventHandler(this.btnService_Click);
            // 
            // btnGenContext
            // 
            this.btnGenContext.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenContext.Location = new System.Drawing.Point(251, 42);
            this.btnGenContext.Name = "btnGenContext";
            this.btnGenContext.Size = new System.Drawing.Size(75, 23);
            this.btnGenContext.TabIndex = 5;
            this.btnGenContext.Text = "Context";
            this.btnGenContext.UseVisualStyleBackColor = true;
            this.btnGenContext.Click += new System.EventHandler(this.btnGenContext_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(9, 20);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(74, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Output Folder:";
            // 
            // btnGenEntityMap
            // 
            this.btnGenEntityMap.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenEntityMap.Location = new System.Drawing.Point(89, 42);
            this.btnGenEntityMap.Name = "btnGenEntityMap";
            this.btnGenEntityMap.Size = new System.Drawing.Size(75, 23);
            this.btnGenEntityMap.TabIndex = 5;
            this.btnGenEntityMap.Text = "Entity Map";
            this.btnGenEntityMap.UseVisualStyleBackColor = true;
            this.btnGenEntityMap.Click += new System.EventHandler(this.btnGenEntityMap_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.lstTable);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(9, 131);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(240, 452);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select table";
            // 
            // lstTable
            // 
            this.lstTable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lstTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstTable.FormattingEnabled = true;
            this.lstTable.Location = new System.Drawing.Point(6, 22);
            this.lstTable.Name = "lstTable";
            this.lstTable.Size = new System.Drawing.Size(228, 420);
            this.lstTable.TabIndex = 0;
            this.lstTable.SelectedIndexChanged += new System.EventHandler(this.lstTable_SelectedIndexChanged);
            // 
            // btnConnString
            // 
            this.btnConnString.Location = new System.Drawing.Point(179, 21);
            this.btnConnString.Name = "btnConnString";
            this.btnConnString.Size = new System.Drawing.Size(30, 23);
            this.btnConnString.TabIndex = 10;
            this.btnConnString.Text = "...";
            this.btnConnString.UseVisualStyleBackColor = true;
            this.btnConnString.Click += new System.EventHandler(this.btnConnString_Click);
            // 
            // cbConnection
            // 
            this.cbConnection.DisplayMember = "Name";
            this.cbConnection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbConnection.FormattingEnabled = true;
            this.cbConnection.Location = new System.Drawing.Point(9, 23);
            this.cbConnection.Name = "cbConnection";
            this.cbConnection.Size = new System.Drawing.Size(164, 21);
            this.cbConnection.TabIndex = 9;
            this.cbConnection.ValueMember = "Id";
            this.cbConnection.SelectedIndexChanged += new System.EventHandler(this.cbConnection_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Database Connection";
            // 
            // tabSettings
            // 
            this.tabSettings.Controls.Add(this.label9);
            this.tabSettings.Controls.Add(this.label10);
            this.tabSettings.Controls.Add(this.label8);
            this.tabSettings.Controls.Add(this.label7);
            this.tabSettings.Controls.Add(this.label3);
            this.tabSettings.Controls.Add(this.label4);
            this.tabSettings.Controls.Add(this.txtContextName);
            this.tabSettings.Controls.Add(this.txtDomainContextNs);
            this.tabSettings.Controls.Add(this.txtDomainModelNs);
            this.tabSettings.Controls.Add(this.txtDomainRepoNs);
            this.tabSettings.Controls.Add(this.txtDomainConfigurationNs);
            this.tabSettings.Controls.Add(this.txtDomainEntityNs);
            this.tabSettings.Controls.Add(this.label12);
            this.tabSettings.Controls.Add(this.txtCoreAutoMapperNs);
            this.tabSettings.Controls.Add(this.label11);
            this.tabSettings.Controls.Add(this.txtCoreContextNs);
            this.tabSettings.Controls.Add(this.label6);
            this.tabSettings.Controls.Add(this.txtCoreRepoNs);
            this.tabSettings.Controls.Add(this.label5);
            this.tabSettings.Controls.Add(this.txtCoreUowNs);
            this.tabSettings.Controls.Add(this.label2);
            this.tabSettings.Controls.Add(this.txtCoreEntityNs);
            this.tabSettings.Location = new System.Drawing.Point(4, 22);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabSettings.Size = new System.Drawing.Size(1037, 589);
            this.tabSettings.TabIndex = 1;
            this.tabSettings.Text = "Settings";
            this.tabSettings.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 282);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Context";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 308);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Context Name";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 244);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Model";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 218);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Repo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 192);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Mapping";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 166);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Entity";
            // 
            // txtContextName
            // 
            this.txtContextName.Location = new System.Drawing.Point(105, 305);
            this.txtContextName.Name = "txtContextName";
            this.txtContextName.Size = new System.Drawing.Size(315, 20);
            this.txtContextName.TabIndex = 3;
            this.txtContextName.Text = "EfContext";
            this.txtContextName.TextChanged += new System.EventHandler(this.txtSettings_TextChanged);
            // 
            // txtDomainContextNs
            // 
            this.txtDomainContextNs.Location = new System.Drawing.Point(105, 279);
            this.txtDomainContextNs.Name = "txtDomainContextNs";
            this.txtDomainContextNs.Size = new System.Drawing.Size(315, 20);
            this.txtDomainContextNs.TabIndex = 3;
            this.txtDomainContextNs.Text = "Libs.DataAccess.Context";
            this.txtDomainContextNs.TextChanged += new System.EventHandler(this.txtSettings_TextChanged);
            // 
            // txtDomainModelNs
            // 
            this.txtDomainModelNs.Location = new System.Drawing.Point(105, 241);
            this.txtDomainModelNs.Name = "txtDomainModelNs";
            this.txtDomainModelNs.Size = new System.Drawing.Size(315, 20);
            this.txtDomainModelNs.TabIndex = 3;
            this.txtDomainModelNs.Text = "Libs.Models";
            this.txtDomainModelNs.TextChanged += new System.EventHandler(this.txtSettings_TextChanged);
            // 
            // txtDomainRepoNs
            // 
            this.txtDomainRepoNs.Location = new System.Drawing.Point(105, 215);
            this.txtDomainRepoNs.Name = "txtDomainRepoNs";
            this.txtDomainRepoNs.Size = new System.Drawing.Size(315, 20);
            this.txtDomainRepoNs.TabIndex = 3;
            this.txtDomainRepoNs.Text = "Libs.Services";
            this.txtDomainRepoNs.TextChanged += new System.EventHandler(this.txtSettings_TextChanged);
            // 
            // txtDomainConfigurationNs
            // 
            this.txtDomainConfigurationNs.Location = new System.Drawing.Point(105, 189);
            this.txtDomainConfigurationNs.Name = "txtDomainConfigurationNs";
            this.txtDomainConfigurationNs.Size = new System.Drawing.Size(315, 20);
            this.txtDomainConfigurationNs.TabIndex = 3;
            this.txtDomainConfigurationNs.Text = "Libs.DataAccess.Configurations";
            this.txtDomainConfigurationNs.TextChanged += new System.EventHandler(this.txtSettings_TextChanged);
            // 
            // txtDomainEntityNs
            // 
            this.txtDomainEntityNs.Location = new System.Drawing.Point(105, 163);
            this.txtDomainEntityNs.Name = "txtDomainEntityNs";
            this.txtDomainEntityNs.Size = new System.Drawing.Size(315, 20);
            this.txtDomainEntityNs.TabIndex = 3;
            this.txtDomainEntityNs.Text = "Libs.DataAccess.Entities";
            this.txtDomainEntityNs.TextChanged += new System.EventHandler(this.txtSettings_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 120);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Core AutoMapper";
            // 
            // txtCoreAutoMapperNs
            // 
            this.txtCoreAutoMapperNs.Location = new System.Drawing.Point(105, 117);
            this.txtCoreAutoMapperNs.Name = "txtCoreAutoMapperNs";
            this.txtCoreAutoMapperNs.Size = new System.Drawing.Size(315, 20);
            this.txtCoreAutoMapperNs.TabIndex = 3;
            this.txtCoreAutoMapperNs.Text = "TIEKinetix.SmartValidator.Common.Infrastructure.Mapping";
            this.txtCoreAutoMapperNs.TextChanged += new System.EventHandler(this.txtSettings_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 92);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Core Context";
            // 
            // txtCoreContextNs
            // 
            this.txtCoreContextNs.Location = new System.Drawing.Point(105, 89);
            this.txtCoreContextNs.Name = "txtCoreContextNs";
            this.txtCoreContextNs.Size = new System.Drawing.Size(315, 20);
            this.txtCoreContextNs.TabIndex = 3;
            this.txtCoreContextNs.Text = "Libcore.DataAccess.Context";
            this.txtCoreContextNs.TextChanged += new System.EventHandler(this.txtSettings_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Core Repo";
            // 
            // txtCoreRepoNs
            // 
            this.txtCoreRepoNs.Location = new System.Drawing.Point(105, 64);
            this.txtCoreRepoNs.Name = "txtCoreRepoNs";
            this.txtCoreRepoNs.Size = new System.Drawing.Size(315, 20);
            this.txtCoreRepoNs.TabIndex = 3;
            this.txtCoreRepoNs.Text = "Libcore.DataAccess.Entities";
            this.txtCoreRepoNs.TextChanged += new System.EventHandler(this.txtSettings_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Core Uow";
            // 
            // txtCoreUowNs
            // 
            this.txtCoreUowNs.Location = new System.Drawing.Point(105, 37);
            this.txtCoreUowNs.Name = "txtCoreUowNs";
            this.txtCoreUowNs.Size = new System.Drawing.Size(315, 20);
            this.txtCoreUowNs.TabIndex = 3;
            this.txtCoreUowNs.Text = "Libcore.DataAccess.Context";
            this.txtCoreUowNs.TextChanged += new System.EventHandler(this.txtSettings_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Core Entity";
            // 
            // txtCoreEntityNs
            // 
            this.txtCoreEntityNs.Location = new System.Drawing.Point(105, 11);
            this.txtCoreEntityNs.Name = "txtCoreEntityNs";
            this.txtCoreEntityNs.Size = new System.Drawing.Size(315, 20);
            this.txtCoreEntityNs.TabIndex = 3;
            this.txtCoreEntityNs.Text = "Libcore.DataAccess.Entities";
            this.txtCoreEntityNs.TextChanged += new System.EventHandler(this.txtSettings_TextChanged);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1058, 623);
            this.Controls.Add(this.tabParent);
            this.Name = "FrmMain";
            this.Text = "...";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.tabParent.ResumeLayout(false);
            this.tabBasic.ResumeLayout(false);
            this.tabBasic.PerformLayout();
            this.tabOutput.ResumeLayout(false);
            this.tabEntity.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtEntityGenerator)).EndInit();
            this.tabMap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMapGenerator)).EndInit();
            this.tabRepo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtRepoGenerator)).EndInit();
            this.tabModel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtModelGenerator)).EndInit();
            this.tabContext.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtContextGenerator)).EndInit();
            this.gbFunctions.ResumeLayout(false);
            this.gbFunctions.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tabSettings.ResumeLayout(false);
            this.tabSettings.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnGenAll;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnGenModel;
        private System.Windows.Forms.Button btnGenEntity;
        private System.Windows.Forms.TabControl tabParent;
        private System.Windows.Forms.TabPage tabBasic;
        private System.Windows.Forms.TabPage tabSettings;
        private System.Windows.Forms.ComboBox cbConnection;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnConnString;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox lstTable;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCoreEntityNs;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtDomainEntityNs;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDomainConfigurationNs;
        private System.Windows.Forms.Button btnGenEntityMap;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCoreUowNs;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDomainRepoNs;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCoreRepoNs;
        private System.Windows.Forms.TabControl tabOutput;
        private System.Windows.Forms.TabPage tabEntity;
        private System.Windows.Forms.TabPage tabMap;
        private FastColoredTextBoxNS.FastColoredTextBox txtEntityGenerator;
        private FastColoredTextBoxNS.FastColoredTextBox txtMapGenerator;
        private System.Windows.Forms.TabPage tabRepo;
        private FastColoredTextBoxNS.FastColoredTextBox txtRepoGenerator;
        private System.Windows.Forms.TabPage tabModel;
        private FastColoredTextBoxNS.FastColoredTextBox txtModelGenerator;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtDomainModelNs;
        private System.Windows.Forms.GroupBox gbFunctions;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtDomainContextNs;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtContextName;
        private System.Windows.Forms.TabPage tabContext;
        private FastColoredTextBoxNS.FastColoredTextBox txtContextGenerator;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtCoreContextNs;
        private System.Windows.Forms.Button btnGenContext;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtCoreAutoMapperNs;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.LinkLabel lblOutputFolder;
        private System.Windows.Forms.Button btnService;
    }
}

