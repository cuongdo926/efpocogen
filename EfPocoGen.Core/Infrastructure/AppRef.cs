﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using EfPocoGen.Core.Generator;

namespace EfPocoGen.Core.Infrastructure
{
    public class AppRef
    {
        public static bool DisableGeographyTypes = false;
        public static string TargetFrameworkVersion;

        public static readonly string[] NotNullable =
        {
            "string",
            "byte[]",
            "Microsoft.SqlServer.Types.SqlGeography",
            "Microsoft.SqlServer.Types.SqlGeometry",
            "System.Data.Entity.Spatial.DbGeography",
            "System.Data.Entity.Spatial.DbGeometry"
        };

        public static readonly string[] ReservedKeywords =
        {
            "abstract", "as", "base", "bool", "break", "byte", "case", "catch", "char",
            "checked", "class", "const", "continue", "decimal", "default", "delegate", "do",
            "double", "else", "enum", "event", "explicit", "extern", "false", "finally", "fixed",
            "float", "for", "foreach", "goto", "if", "implicit", "in", "int", "interface",
            "internal", "is", "lock", "long", "namespace", "new", "null", "object", "operator",
            "out", "override", "params", "private", "protected", "public", "readonly", "ref",
            "return", "sbyte", "sealed", "short", "sizeof", "stackalloc", "static", "string",
            "struct", "switch", "this", "throw", "true", "try", "typeof", "uint", "ulong",
            "unchecked", "unsafe", "ushort", "using", "virtual", "volatile", "void", "while"
        };

        public static readonly Regex RxCleanUp = new Regex(@"[^\w\d_]", RegexOptions.Compiled);

        public static readonly Func<string, string> CleanUp = str =>
        {
            // Replace punctuation and symbols in variable names as these are not allowed.
            int len = str.Length;
            if (len == 0)
                return str;
            var sb = new StringBuilder();
            bool replacedCharacter = false;
            for (int n = 0; n < len; ++n)
            {
                char c = str[n];
                if (c != '_' && (char.IsSymbol(c) || char.IsPunctuation(c)))
                {
                    int ascii = c;
                    sb.AppendFormat("{0}", ascii);
                    replacedCharacter = true;
                    continue;
                }
                sb.Append(c);
            }
            if (replacedCharacter)
                str = sb.ToString();

            // Remove non alphanumerics
            str = RxCleanUp.Replace(str, "");
            if (char.IsDigit(str[0]))
                str = "C" + str;

            return str;
        };

        private readonly Regex ColumnFilterExclude = null;
        public string SchemaName = "dbo";
        private readonly Regex StoredProcedureFilterExclude = null;
        private readonly Regex StoredProcedureFilterInclude = null;
        private readonly Regex TableFilterExclude = new Regex(".*sysdiagram.*");
        private readonly Regex TableFilterInclude = null;

        // Settings - edit these in the main <name>.tt file *******************************************************************************
        private bool AddUnitTestingDbContext = true;
        private bool AddWcfDataAttributes = false;
        private string[] AdditionalContextInterfaceItems = null;
        private string[] AdditionalNamespaces = null;
        private bool AutomaticMigrationDataLossAllowed = true;
        private bool AutomaticMigrationsEnabled = true;
        private string CodeGeneratedAttribute = "[GeneratedCodeAttribute(\"EF.Reverse.POCO.Generator\", \"2.13.0.0\")]";
        private string CollectionType = "List";
        private string CollectionTypeNamespace = "";
        private string[] ConfigFilenameSearchOrder = null;
        private string ConnectionStringName = "";


        public static string Output = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), "Generated");

        private Elements ElementsToGenerate = Elements.Poco | Elements.Context | Elements.UnitOfWork |
                                              Elements.PocoConfiguration | Elements.StoredProcedures;

        private string ExtraWcfDataContractAttributes = "";

        private string FileExtension = ".cs";
        private bool GenerateSeparateFiles = false;
        private bool IncludeComments = false;

        private ExtendedPropertyCommentsStyle IncludeExtendedPropertyComments =
            ExtendedPropertyCommentsStyle.InSummaryBlock;

        private bool IncludeQueryTraceOn9481Flag = false;
        private bool IncludeViews = true;

        private bool MakeClassesPartial = true;

        private string MigrationConfigurationFileName = null;
        private string MigrationStrategy = "MigrateDatabaseToLatestVersion";
        public string RepositoryInterfaceBaseClass = "IDbSet";
        public string RootNamespace = "";
        public string DbContextInterfaceBaseClasses = "IDisposable";
        public string EditNamespace = "Edit";

        public AppSetting AppSetting = new AppSetting();

        //public string CoreEntityNs = "Core.DataAccess.Entities";
        //public string CoreUowNs = "Core.DataAccess.Uow";
        //public string CoreRepoNs = "Core.DataAccess.Repositories";
        //public string CoreContextNs = "Core.DataAccess.Context";

        //public string ContextName = "EfDbContext";

        //public string DomainContextNs = "Context";
        //public string ValidatorNs = "Validators";
        //public string DomainModelNs = "Models";
        //public string DomainRepoNs = "Repositories";
        //public string DomainEntityNs = "Entities";
        //public string DomainConfigurationNs = "Configurations";

        private bool PrependSchemaName = true;
        private Func<string, string, string> StoredProcedureRename = (name, schema) => name;
        private Func<string, string, string> TableRename = (name, schema) => name;
        private string UnitOfWorkNamespace;
        private Func<Column, Table, Column> UpdateColumn = (column, table) => column;
        private bool UseCamelCase = true;
        private string _configFilePath = "";
        private string _connectionString = "";
        private string _providerName = "System.Data.SqlClient";

        public string ConnectionString
        {
            get { return _connectionString; }
            set { _connectionString = value; }
        }

        public string ProviderName
        {
            get { return _providerName; }
            set { _providerName = value; }
        }

        public static void ArgumentNotNull<T>(T arg, string name) where T : class
        {
            if (arg == null)
            {
                throw new ArgumentNullException(name);
            }
        }

        public string GetInterfaceRepositoryClassName(Table table)
        {
            return string.Format("{0}<{1}>", RepositoryInterfaceBaseClass, table.NameHumanCase);
        }

        public static string CheckNullable(Column col)
        {
            string result = "";
            if (col.IsNullable && !NotNullable.Contains(col.PropertyType, StringComparer.InvariantCultureIgnoreCase))
                result = "?";
            return result;
        }

        private string GetConnectionString(ref string connectionStringName, out string providerName,
            out string configFilePath)
        {
            providerName = null;
            configFilePath = null;
            return "";
        }

        private void InitConnectionString()
        {
            if (!String.IsNullOrEmpty(_connectionString))
                return;

            _connectionString = GetConnectionString(ref ConnectionStringName, out _providerName, out _configFilePath);
        }

        private static string ZapPassword(string connectionString)
        {
            var rx = new Regex("password=.*;",
                RegexOptions.Singleline | RegexOptions.Multiline | RegexOptions.IgnoreCase);
            return rx.Replace(connectionString, "password=**zapped**;");
        }

        public DbProviderFactory GetDbProviderFactory()
        {
            InitConnectionString();

            try
            {
                return DbProviderFactories.GetFactory(ProviderName);
            }
            catch (Exception x)
            {
                string error = x.Message.Replace("\r\n", "\n").Replace("\n", " ");
                Warning(string.Format("Failed to load provider \"{0}\" - {1}", ProviderName, error));
                WriteLine("");
                WriteLine("// -----------------------------------------------------------------------------------------");
                WriteLine("// Failed to load provider \"{0}\" - {1}", ProviderName, error);
                WriteLine("// -----------------------------------------------------------------------------------------");
                WriteLine("");
                return null;
            }
        }

        private DbProviderFactory TryGetDbProviderFactory()
        {
            try
            {
                return DbProviderFactories.GetFactory(ProviderName);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Tables AllTables { get; private set; }

        public Tables LoadTables(DbProviderFactory factory)
        {
            if (factory == null || !(ElementsToGenerate.HasFlag(Elements.Poco) ||
                                     ElementsToGenerate.HasFlag(Elements.Context) ||
                                     ElementsToGenerate.HasFlag(Elements.UnitOfWork) ||
                                     ElementsToGenerate.HasFlag(Elements.PocoConfiguration)))
            {
                AllTables = new Tables();
                return AllTables;
            }

            try
            {
                using (DbConnection conn = factory.CreateConnection())
                {
                    conn.ConnectionString = ConnectionString;
                    conn.Open();

                    bool isSqlCE = false;
                    if (conn.GetType().Name == "SqlCeConnection")
                    {
                        PrependSchemaName = false;
                        isSqlCE = true;
                    }


                    var reader = new SqlServerSchemaReader(conn, factory, IncludeQueryTraceOn9481Flag) { Outer = this };
                    Tables tables = reader.ReadSchema(TableFilterExclude, ColumnFilterExclude, UseCamelCase,
                        PrependSchemaName, IncludeComments, IncludeExtendedPropertyComments, TableRename, SchemaName,
                        UpdateColumn);
                    tables.SetPrimaryKeys();

                    // Remove unrequired tables/views
                    for (int i = tables.Count - 1; i >= 0; i--)
                    {
                        if (SchemaName != null &&
                            String.Compare(tables[i].Schema, SchemaName, StringComparison.OrdinalIgnoreCase) != 0)
                        {
                            tables.RemoveAt(i);
                            continue;
                        }
                        if (!IncludeViews && tables[i].IsView)
                        {
                            tables.RemoveAt(i);
                            continue;
                        }
                        if (TableFilterInclude != null && !TableFilterInclude.IsMatch(tables[i].Name))
                        {
                            tables.RemoveAt(i);
                            continue;
                        }
                        if (!tables[i].IsView && string.IsNullOrEmpty(tables[i].PrimaryKeyNameHumanCase()))
                        {
                            tables.RemoveAt(i);
                        }
                    }

                    // Must be done in this order
                    List<ForeignKey> fkList = reader.ReadForeignKeys(TableRename);
                    reader.IdentifyForeignKeys(fkList, tables);
                    reader.ProcessForeignKeys(fkList, tables, UseCamelCase, PrependSchemaName, CollectionType, true,
                        IncludeComments);
                    tables.IdentifyMappingTables(fkList, UseCamelCase, CollectionType, true, IncludeComments, isSqlCE);

                    tables.ResetNavigationProperties();
                    reader.ProcessForeignKeys(fkList, tables, UseCamelCase, PrependSchemaName, CollectionType, false,
                        IncludeComments);
                    tables.IdentifyMappingTables(fkList, UseCamelCase, CollectionType, false, IncludeComments, isSqlCE);

                    // Remove views that only consist of all nullable fields.
                    // I.e. they do not contain any primary key, and therefore cannot be used by EF
                    for (int i = tables.Count - 1; i >= 0; i--)
                    {
                        if (string.IsNullOrEmpty(tables[i].PrimaryKeyNameHumanCase()))
                        {
                            tables.RemoveAt(i);
                        }
                    }

                    conn.Close();
                    AllTables = tables;
                }
            }
            catch (Exception x)
            {
                string error = x.Message.Replace("\r\n", "\n").Replace("\n", " ");
                Warning(string.Format("Failed to read database schema - {0}", error));
                WriteLine("");
                WriteLine("// -----------------------------------------------------------------------------------------");
                WriteLine("// Failed to read database schema - {0}", error);
                WriteLine("// -----------------------------------------------------------------------------------------");
                WriteLine("");

                AllTables = new Tables();
            }
            return AllTables;
        }

        public List<StoredProcedure> LoadStoredProcs(DbProviderFactory factory)
        {
            if (factory == null || !ElementsToGenerate.HasFlag(Elements.StoredProcedures))
                return new List<StoredProcedure>();

            try
            {
                using (DbConnection conn = factory.CreateConnection())
                {
                    conn.ConnectionString = ConnectionString;
                    conn.Open();

                    if (conn.GetType().Name == "SqlCeConnection")
                        return new List<StoredProcedure>();

                    var reader = new SqlServerSchemaReader(conn, factory, IncludeQueryTraceOn9481Flag) { Outer = this };
                    List<StoredProcedure> storedProcs = reader.ReadStoredProcs(StoredProcedureFilterExclude,
                        UseCamelCase, PrependSchemaName, StoredProcedureRename, SchemaName);

                    // Remove unrequired stored procs
                    for (int i = storedProcs.Count - 1; i >= 0; i--)
                    {
                        if (SchemaName != null &&
                            String.Compare(storedProcs[i].Schema, SchemaName, StringComparison.OrdinalIgnoreCase) != 0)
                        {
                            storedProcs.RemoveAt(i);
                            continue;
                        }
                        if (StoredProcedureFilterInclude != null &&
                            !StoredProcedureFilterInclude.IsMatch(storedProcs[i].Name))
                        {
                            storedProcs.RemoveAt(i);
                        }
                    }

                    foreach (StoredProcedure proc in storedProcs)
                        reader.ReadStoredProcReturnObject(ConnectionString, proc);

                    conn.Close();

                    // Remove stored procs where the return model type contains spaces and cannot be mapped
                    var validStoredProcedures = new List<StoredProcedure>();
                    foreach (StoredProcedure sp in storedProcs)
                    {
                        if (sp.ReturnColumns.Count() == 0)
                        {
                            validStoredProcedures.Add(sp);
                            continue;
                        }
                        if (sp.ReturnColumns.Any(c => c.ColumnName.Contains(" ")))
                            continue; // Return column contains a space. Ignore as this cannot be mapped.
                        validStoredProcedures.Add(sp);
                    }
                    return validStoredProcedures;
                }
            }
            catch (Exception x)
            {
                string error = x.Message.Replace("\r\n", "\n").Replace("\n", " ");
                Warning(string.Format("Failed to read database schema for stored procedures - {0}", error));
                WriteLine("");
                WriteLine("// -----------------------------------------------------------------------------------------");
                WriteLine("// Failed to read database schema for stored procedures - {0}", error);
                WriteLine("// -----------------------------------------------------------------------------------------");
                WriteLine("");
                return new List<StoredProcedure>();
            }
        }

        private void Warning(string format)
        {
            Console.WriteLine(format);
        }

        public static Relationship CalcRelationship(Table pkTable, Table fkTable, List<Column> fkCols,
            List<Column> pkCols)
        {
            if (fkCols.Count() == 1 && pkCols.Count() == 1)
                return CalcRelationshipSingle(pkTable, fkTable, fkCols.First(), pkCols.First());

            // This relationship has multiple composite keys
            return Relationship.DoNotUse;

            /*bool fkTableAllPrimaryKeys = (fkTable.PrimaryKeys.Count() == fkCols.Count());
            bool pkTableAllPrimaryKeys = (pkTable.PrimaryKeys.Count() == pkCols.Count());
            bool fkColumnsAllPrimaryKeys = (fkCols.Count(x => x.IsPrimaryKey) == fkCols.Count());
            bool pkColumnsAllPrimaryKeys = (pkCols.Count(x => x.IsPrimaryKey) == pkCols.Count());

            // 1:1
            if(fkColumnsAllPrimaryKeys && pkColumnsAllPrimaryKeys && fkTableAllPrimaryKeys && pkTableAllPrimaryKeys)
                return Relationship.OneToOne;

            // 1:n
            if(fkColumnsAllPrimaryKeys && !pkColumnsAllPrimaryKeys && fkTableAllPrimaryKeys)
                return Relationship.OneToMany;

            // n:1
            if(!fkColumnsAllPrimaryKeys && pkColumnsAllPrimaryKeys && pkTableAllPrimaryKeys)
                return Relationship.ManyToOne;

            // n:n
            return Relationship.ManyToMany;*/
        }

        public static Relationship CalcRelationshipSingle(Table pkTable, Table fkTable, Column fkCol, Column pkCol)
        {
            bool fkTableSinglePrimaryKey = (fkTable.PrimaryKeys.Count() == 1);
            bool pkTableSinglePrimaryKey = (pkTable.PrimaryKeys.Count() == 1);

            // 1:1
            if (fkCol.IsPrimaryKey && pkCol.IsPrimaryKey && fkTableSinglePrimaryKey && pkTableSinglePrimaryKey)
                return Relationship.OneToOne;

            // 1:n
            if (fkCol.IsPrimaryKey && !pkCol.IsPrimaryKey && fkTableSinglePrimaryKey)
                return Relationship.OneToMany;

            // n:1
            if (!fkCol.IsPrimaryKey && pkCol.IsPrimaryKey && pkTableSinglePrimaryKey)
                return Relationship.ManyToOne;

            // n:n
            return Relationship.ManyToMany;
        }

        public void WriteLine(string s, params object[] parameters)
        {
            Console.WriteLine(s, parameters);
        }
    }
}