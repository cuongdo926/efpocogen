using System;
using System.Data.Entity.Infrastructure.Pluralization;
using System.Text.RegularExpressions;

namespace EfPocoGen.Core.Infrastructure
{
    /// <summary>
    ///     Summary for the Inflector class
    /// </summary>
    public static class Inflector
    {
        public static IPluralizationService PluralizationService = null;

        static Inflector()
        {
            PluralizationService = new EnglishPluralizationService(new[]
			{
				// Create custom ("Singular", "Plural") forms for one-off words as needed
		
				new CustomPluralizationEntry("LiveQuiz", "LiveQuizzes"),
				new CustomPluralizationEntry("Course", "Courses"),
				new CustomPluralizationEntry("CustomerStatus", "CustomerStatus"), // Use same value to prevent pluralisation
				new CustomPluralizationEntry("EmployeeStatus", "EmployeeStatus"),
			});
        }

        /// <summary>
        ///     Makes the plural.
        /// </summary>
        /// <param name="word">The word.</param>
        /// <returns></returns>
        public static string MakePlural(string word)
        {
            try
            {
                if (word.EndsWith("Alia"))
                {
                    return word + "ses";
                }
                if (word.EndsWith("Alias"))
                {
                    return word + "es";
                }
                return (PluralizationService == null) ? word : PluralizationService.Pluralize(word);
            }
            catch (Exception)
            {
                return word;
            }
        }

        /// <summary>
        ///     Makes the singular.
        /// </summary>
        /// <param name="word">The word.</param>
        /// <returns></returns>
        public static string MakeSingular(string word)
        {
            try
            {
                if (word.EndsWith("Alias"))
                {
                    return word;
                }
                return (PluralizationService == null) ? word : PluralizationService.Singularize(word);
            }
            catch (Exception)
            {
                return word;
            }
        }

        /// <summary>
        ///     Converts the string to title case.
        /// </summary>
        /// <param name="word">The word.</param>
        /// <returns></returns>
        public static string ToTitleCase(string word)
        {
            string s = Regex.Replace(ToHumanCase(AddUnderscores(word)), @"\b([a-z])",
                match => match.Captures[0].Value.ToUpper());
            bool digit = false;
            string a = String.Empty;
            foreach (char c in s)
            {
                if (Char.IsDigit(c))
                {
                    digit = true;
                    a = a + c;
                }
                else
                {
                    if (digit && Char.IsLower(c))
                        a = a + Char.ToUpper(c);
                    else
                        a = a + c;
                    digit = false;
                }
            }
            return a;
        }

        /// <summary>
        ///     Converts the string to human case.
        /// </summary>
        /// <param name="lowercaseAndUnderscoredWord">The lowercase and underscored word.</param>
        /// <returns></returns>
        public static string ToHumanCase(string lowercaseAndUnderscoredWord)
        {
            return MakeInitialCaps(Regex.Replace(lowercaseAndUnderscoredWord, @"_", " "));
        }


        /// <summary>
        ///     Adds the underscores.
        /// </summary>
        /// <param name="pascalCasedWord">The pascal cased word.</param>
        /// <returns></returns>
        public static string AddUnderscores(string pascalCasedWord)
        {
            return
                Regex.Replace(
                    Regex.Replace(Regex.Replace(pascalCasedWord, @"([A-Z]+)([A-Z][a-z])", "$1_$2"),
                        @"([a-z\d])([A-Z])", "$1_$2"), @"[-\s]", "_").ToLower();
        }

        /// <summary>
        ///     Makes the initial caps.
        /// </summary>
        /// <param name="word">The word.</param>
        /// <returns></returns>
        public static string MakeInitialCaps(string word)
        {
            return String.Concat(word.Substring(0, 1).ToUpper(), word.Substring(1).ToLower());
        }

        /// <summary>
        ///     Makes the initial character lowercase.
        /// </summary>
        /// <param name="word">The word.</param>
        /// <returns></returns>
        public static string MakeInitialLower(string word)
        {
            return String.Concat(word.Substring(0, 1).ToLower(), word.Substring(1));
        }
    }
}