﻿using System.IO;
using EfPocoGen.Core.Generator;
using Microsoft.VisualBasic.FileIO;

namespace EfPocoGen.Core.Infrastructure
{
    public class FileManager
    {
        public AppRef AppRef { get; private set; }

        public FileManager(AppRef appRef)
        {
            AppRef = appRef;
        }

        public void WriteToFile(AbstractGenerator generator)
        {
            var fileName = Path.Combine(AppRef.Output, GetFileName(generator));
            var directory = Path.GetDirectoryName(fileName);
            if (directory != null && !FileSystem.DirectoryExists(directory))
            {
                FileSystem.CreateDirectory(directory);
            }

            FileSystem.WriteAllText(fileName, generator.ToString(), false);
        }

        private static string GetFileName(AbstractGenerator generator)
        {
            var folder = generator.FullNamespace.Substring(generator.FullNamespace.LastIndexOf(".", System.StringComparison.Ordinal) + 1);

            return folder + "\\" + generator.FileName;
        }
    }
}