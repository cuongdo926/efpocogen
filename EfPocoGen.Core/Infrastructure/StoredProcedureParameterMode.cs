namespace EfPocoGen.Core.Infrastructure
{
    public enum StoredProcedureParameterMode
    {
        In,
        InOut,
        Out
    };
}