namespace EfPocoGen.Core.Infrastructure
{
    public class StoredProcedureParameter
    {
        public int DateTimePrecision;
        public int MaxLength;
        public StoredProcedureParameterMode Mode;
        public string Name;
        public string NameHumanCase;
        public int Ordinal;
        public int Precision;
        public string PropertyType;
        public int Scale;
        public string SqlDbType;
    }
}