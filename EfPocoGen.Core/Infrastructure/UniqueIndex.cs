namespace EfPocoGen.Core.Infrastructure
{
    public class UniqueIndex
    {
        public string Column;
        public int ColumnCount;
        public string IndexName;
        public byte KeyOrdinal;
        public string Schema;
        public string TableName;
    }
}