using System;
using System.Collections.Generic;
using System.Linq;

namespace EfPocoGen.Core.Infrastructure
{
    public class Tables : List<Table>
    {
        public Table GetTable(string tableName, string schema)
        {
            return this.SingleOrDefault(x =>
                String.Compare(x.Name, tableName, StringComparison.OrdinalIgnoreCase) == 0 &&
                String.Compare(x.Schema, schema, StringComparison.OrdinalIgnoreCase) == 0);
        }

        public void SetPrimaryKeys()
        {
            foreach (Table tbl in this)
            {
                tbl.SetPrimaryKeys();
            }
        }

        public void IdentifyMappingTables(List<ForeignKey> fkList, bool useCamelCase, string collectionType,
            bool checkForFkNameClashes, bool includeComments, bool isSqlCE)
        {
            foreach (Table tbl in this.Where(x => x.HasForeignKey))
            {
                tbl.IdentifyMappingTable(fkList, this, useCamelCase, collectionType, checkForFkNameClashes,
                    includeComments, isSqlCE);
            }
        }

        public void ResetNavigationProperties()
        {
            foreach (Table tbl in this)
            {
                tbl.ResetNavigationProperties();
            }
        }
    }
}