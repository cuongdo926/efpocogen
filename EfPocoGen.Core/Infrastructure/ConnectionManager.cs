﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace EfPocoGen.Core.Infrastructure
{
    public class ConnectionManager
    {
        public List<Connection> Connections { get; private set; }

        public ConnectionManager()
        {
            Connections = new List<Connection>();
        }

        public void Add(Connection connection)
        {
            Connections.Add(connection);
        }

        public void Remove(Connection connection)
        {
            Connections.Remove(connection);
        }

        public string ToJsonString()
        {
            return JsonConvert.SerializeObject(Connections);
        }

        public void Load(string input)
        {
            try
            {
                Connections = JsonConvert.DeserializeObject<List<Connection>>(input);
            }
            catch
            {
            }

            if (Connections == null)
            {
                Connections = new List<Connection>();
            }
        }
    }
}
