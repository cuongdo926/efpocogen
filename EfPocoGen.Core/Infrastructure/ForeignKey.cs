using System;

namespace EfPocoGen.Core.Infrastructure
{
    public class ForeignKey
    {
        public ForeignKey(string fkTableName, string fkSchema, string pkTableName, string pkSchema, string fkColumn,
            string pkColumn, string constraintName, string fkTableNameFiltered, string pkTableNameFiltered,
            int ordinal)
        {
            ConstraintName = constraintName;
            PkColumn = pkColumn;
            FkColumn = fkColumn;
            PkSchema = pkSchema;
            PkTableName = pkTableName;
            FkSchema = fkSchema;
            FkTableName = fkTableName;
            FkTableNameFiltered = fkTableNameFiltered;
            PkTableNameFiltered = pkTableNameFiltered;
            Ordinal = ordinal;
        }

        public string FkTableName { get; private set; }
        public string FkTableNameFiltered { get; private set; }
        public string FkSchema { get; private set; }
        public string PkTableName { get; private set; }
        public string PkTableNameFiltered { get; private set; }
        public string PkSchema { get; private set; }
        public string FkColumn { get; private set; }
        public string PkColumn { get; private set; }
        public string ConstraintName { get; private set; }
        public int Ordinal { get; private set; }

        public string PkTableHumanCase(bool useCamelCase, bool prependSchemaName)
        {
            string singular = Inflector.MakeSingular(PkTableNameFiltered);
            string pkTableHumanCase =
                (useCamelCase ? Inflector.ToTitleCase(singular) : singular).Replace(" ", "").Replace("$", "");
            if (String.Compare(PkSchema, "dbo", StringComparison.OrdinalIgnoreCase) != 0 && prependSchemaName)
                pkTableHumanCase = PkSchema + "_" + pkTableHumanCase;
            return pkTableHumanCase;
        }
    }
}