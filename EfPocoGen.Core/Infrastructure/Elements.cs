using System;

namespace EfPocoGen.Core.Infrastructure
{
    [Flags]
    internal enum Elements
    {
        None = 0,
        Poco = 1,
        Context = 2,
        UnitOfWork = 4,
        PocoConfiguration = 8,
        StoredProcedures = 16
    };
}