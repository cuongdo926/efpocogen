using Newtonsoft.Json;

namespace EfPocoGen.Core.Infrastructure
{
    public class AppSetting
    {
        public string CoreAutoMapperNs = "Common.Infrastructure.Mapping";
        public string ContextName = "EfContext";
        public string DomainEntityNs = "Entities";

        public string ToJsonString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static AppSetting Load(string input)
        {
            try
            {
                return JsonConvert.DeserializeObject<AppSetting>(input);
            }
            catch
            {
                return new AppSetting();
            }
        }
    }
}