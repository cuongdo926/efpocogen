﻿namespace EfPocoGen.Core.Infrastructure
{
    public enum AuditType
    {
        None,
        Creation,
        Modification,
    }
}
