using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text.RegularExpressions;

namespace EfPocoGen.Core.Infrastructure
{
    public abstract class SchemaReader
    {
        protected readonly DbCommand Cmd;

        public AppRef Outer;

        protected SchemaReader(DbConnection connection, DbProviderFactory factory)
        {
            Cmd = factory.CreateCommand();
            if (Cmd != null)
                Cmd.Connection = connection;
        }

        public abstract Tables ReadSchema(Regex tableFilterExclude, Regex columnFilterExclude, bool useCamelCase,
            bool prependSchemaName, bool includeComments,
            ExtendedPropertyCommentsStyle includeExtendedPropertyComments, Func<string, string, string> tableRename,
            string schemaNameFilter, Func<Column, Table, Column> updateColumn);

        public abstract List<StoredProcedure> ReadStoredProcs(Regex storedProcedureFilterExclude, bool useCamelCase,
            bool prependSchemaName, Func<string, string, string> StoredProcedureRename, string schemaNameFilter);

        public abstract List<ForeignKey> ReadForeignKeys(Func<string, string, string> tableRename);

        public abstract void ProcessForeignKeys(List<ForeignKey> fkList, Tables tables, bool useCamelCase,
            bool prependSchemaName, string collectionType, bool checkForFkNameClashes, bool includeComments);

        public abstract void IdentifyForeignKeys(List<ForeignKey> fkList, Tables tables);
        public abstract void ReadUniqueIndexes(Tables tables);
        public abstract void ReadExtendedProperties(Tables tables);

        protected void WriteLine(string o)
        {
            Outer.WriteLine(o);
        }
    }
}