using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace EfPocoGen.Core.Infrastructure
{
    public class StoredProcedure
    {
        public string Name;
        public string NameHumanCase;
        public List<StoredProcedureParameter> Parameters;
        public List<DataColumn> ReturnColumns;
        public string Schema;

        public StoredProcedure()
        {
            Parameters = new List<StoredProcedureParameter>();
            ReturnColumns = new List<DataColumn>();
        }

        public static string CheckNullable(DataColumn col)
        {
            string result = " ";
            if (col.AllowDBNull &&
                !AppRef.NotNullable.Contains(col.DataType.Name, StringComparer.InvariantCultureIgnoreCase))
                result = "? ";
            return result;
        }
    }
}