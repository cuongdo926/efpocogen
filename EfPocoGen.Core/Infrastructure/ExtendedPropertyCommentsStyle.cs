using System;

namespace EfPocoGen.Core.Infrastructure
{
    [Flags]
    public enum ExtendedPropertyCommentsStyle
    {
        None,
        InSummaryBlock,
        AtEndOfField
    };
}