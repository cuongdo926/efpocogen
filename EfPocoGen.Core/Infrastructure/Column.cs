using System;
using System.Linq;

namespace EfPocoGen.Core.Infrastructure
{
    public enum ColumnType
    {
        Normal,
        Id,
        RowVersion,
        Audit,
    }

    public class Column
    {
        public readonly string[] AuditProperties = { "CreatedAt", "ModifiedAt", "CreatedBy", "ModifiedBy" };

        public string Config;
        public string ConfigFk;
        public int DateTimePrecision;
        public string Default;
        public string Entity;
        public string EntityId;
        public string EntityFk;
        public string LookupHandlerFk;
        public string ExtendedProperty;
        public bool Hidden;
        public bool IsFixedLength;
        public bool IsForeignKey;

        public bool IsIdentity;
        public bool IsNullable;
        public bool IsPrimaryKey;
        public bool IsPrimaryKeyViaUniqueIndex;
        public bool IsRowVersion;
        public bool IsStoreGenerated;
        public bool IsUnicode;
        public int MaxLength;
        public string Name;
        public int Ordinal;
        public int Precision;
        public int PrimaryKeyOrdinal;
        public string PropertyName;
        public string PropertyNameHumanCase;
        public string PropertyType;
        public int Scale;
        public string UniqueIndexName;

        public bool IsIdColumn
        {
            get { return PropertyName.Equals("Id"); }
        }

        public ColumnType ColumnType
        {
            get
            {
                if (AuditProperties.Contains(PropertyName))
                    return ColumnType.Audit;
                if (PropertyName.Equals("RowVersion"))
                    return ColumnType.RowVersion;
                if (IsIdColumn)
                    return ColumnType.Id;

                return ColumnType.Normal;
            }

        }

        private void SetupEntity(bool includeComments, ExtendedPropertyCommentsStyle includeExtendedPropertyComments)
        {
            string comments;
            if (includeComments)
            {
                comments = " // " + Name;
                if (IsPrimaryKey)
                {
                    if (IsPrimaryKeyViaUniqueIndex)
                        comments += " (Primary key via unique index " + UniqueIndexName + ")";
                    else
                        comments += " (Primary key)";
                }
            }
            else
            {
                comments = String.Empty;
            }

            if (includeExtendedPropertyComments == ExtendedPropertyCommentsStyle.AtEndOfField &&
                !String.IsNullOrEmpty(ExtendedProperty))
            {
                if (String.IsNullOrEmpty(comments))
                    comments = " // " + ExtendedProperty;
                else
                    comments += ". " + ExtendedProperty;
            }

            Entity = String.Format("public {0}{1} {2} {3}{4}", PropertyType, AppRef.CheckNullable(this),
                PropertyNameHumanCase, "{ get; set; }", comments);

            EntityId = String.Format("public {0}{1} {2} {3}{4}", PropertyType, AppRef.CheckNullable(this),
               "Id", "{ get; set; }", comments);
        }

        private void SetupConfig()
        {
            bool hasDatabaseGeneratedOption = false;
            string propertyType = PropertyType.ToLower();
            switch (propertyType)
            {
                case "long":
                case "short":
                case "int":
                case "double":
                case "float":
                case "decimal":
                case "string":
                    hasDatabaseGeneratedOption = true;
                    break;
            }
            string databaseGeneratedOption = String.Empty;
            if (hasDatabaseGeneratedOption)
            {
                if (IsIdentity)
                    databaseGeneratedOption = ".HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)";
                if (IsStoreGenerated && !IsIdentity)
                    databaseGeneratedOption = ".HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed)";
                if (IsPrimaryKey && !IsIdentity && !IsStoreGenerated)
                    databaseGeneratedOption = ".HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)";
            }
            Config = String.Format("Property(x => x.{0}).HasColumnName(\"{1}\"){2}{3}{4}{5}{6}{7}{8};",
                PropertyNameHumanCase, Name,
                (IsNullable) ? ".IsOptional()" : ".IsRequired()",
                (IsFixedLength || IsRowVersion) ? ".IsFixedLength()" : "",
                (IsUnicode) ? String.Empty : ".IsUnicode(false)",
                (MaxLength > 0) ? ".HasMaxLength(" + MaxLength + ")" : String.Empty,
                (Scale > 0) ? ".HasPrecision(" + Precision + "," + Scale + ")" : String.Empty,
                (IsRowVersion) ? ".IsRowVersion()" : String.Empty,
                databaseGeneratedOption);
        }

        public void SetupEntityAndConfig(bool includeComments,
            ExtendedPropertyCommentsStyle includeExtendedPropertyComments)
        {
            SetupEntity(includeComments, includeExtendedPropertyComments);
            SetupConfig();
        }

        public void CleanUpDefault()
        {
            if (String.IsNullOrEmpty(Default))
                return;

            while (Default.First() == '(' && Default.Last() == ')' && Default.Length > 2)
            {
                Default = Default.Substring(1, Default.Length - 2);
            }

            if (Default.First() == '\'' && Default.Last() == '\'' && Default.Length >= 2)
                Default = String.Format("\"{0}\"", Default.Substring(1, Default.Length - 2));

            string lower = Default.ToLower();
            string lowerPropertyType = PropertyType.ToLower();

            // Cleanup default
            switch (lowerPropertyType)
            {
                case "bool":
                    Default = (Default == "0") ? "false" : "true";
                    break;

                case "string":
                case "datetime":
                case "timespan":
                case "datetimeoffset":
                    if (Default.First() != '"')
                        Default = String.Format("\"{0}\"", Default);
                    if (Default.Contains('\\'))
                        Default = "@" + Default;
                    break;

                case "long":
                case "short":
                case "int":
                case "double":
                case "float":
                case "decimal":
                case "byte":
                case "guid":
                    if (Default.First() == '\"' && Default.Last() == '\"' && Default.Length > 2)
                        Default = Default.Substring(1, Default.Length - 2);
                    break;

                case "byte[]":
                case "System.Data.Entity.Spatial.DbGeography":
                case "System.Data.Entity.Spatial.DbGeometry":
                    Default = String.Empty;
                    break;
            }

            if (String.IsNullOrWhiteSpace(Default))
                return;

            // Validate default
            switch (lowerPropertyType)
            {
                case "long":
                    long l;
                    if (!Int64.TryParse(Default, out l))
                        Default = String.Empty;
                    break;

                case "short":
                    short s;
                    if (!Int16.TryParse(Default, out s))
                        Default = String.Empty;
                    break;

                case "int":
                    int i;
                    if (!Int32.TryParse(Default, out i))
                        Default = String.Empty;
                    break;

                case "datetime":
                    DateTime dt;
                    if (!DateTime.TryParse(Default, out dt))
                        Default = lower.Contains("getdate()")
                            ? "System.DateTime.Now"
                            : lower.Contains("getutcdate()") ? "System.DateTime.UtcNow" : String.Empty;
                    else
                        Default = String.Format("System.DateTime.Parse({0})", Default);
                    break;

                case "datetimeoffset":
                    DateTimeOffset dto;
                    if (!DateTimeOffset.TryParse(Default, out dto))
                        Default = lower.Contains("sysdatetimeoffset")
                            ? "System.DateTimeOffset.Now"
                            : lower.Contains("sysutcdatetime") ? "System.DateTimeOffset.UtcNow" : String.Empty;
                    else
                        Default = String.Format("System.DateTimeOffset.Parse({0})", Default);
                    break;

                case "timespan":
                    TimeSpan ts;
                    if (!TimeSpan.TryParse(Default, out ts))
                        Default = String.Empty;
                    else
                        Default = String.Format("System.TimeSpan.Parse({0})", Default);
                    break;

                case "double":
                    double d;
                    if (!Double.TryParse(Default, out d))
                        Default = String.Empty;
                    break;

                case "float":
                    float f;
                    if (!Single.TryParse(Default, out f))
                        Default = String.Empty;
                    break;

                case "decimal":
                    decimal dec;
                    if (!Decimal.TryParse(Default, out dec))
                        Default = String.Empty;
                    else
                        Default += "m";
                    break;

                case "byte":
                    byte b;
                    if (!Byte.TryParse(Default, out b))
                        Default = String.Empty;
                    break;

                case "bool":
                    bool x;
                    if (!Boolean.TryParse(Default, out x))
                        Default = String.Empty;
                    break;

                case "string":
                    if (lower.Contains("newid()") || lower.Contains("newsequentialid()"))
                        Default = "Guid.NewGuid().ToString()";
                    break;

                case "guid":
                    if (lower.Contains("newid()") || lower.Contains("newsequentialid()"))
                        Default = "System.Guid.NewGuid()";
                    else if (lower.Contains("null"))
                        Default = "null";
                    else
                        Default = String.Format("Guid.Parse(\"{0}\")", Default);
                    break;
            }
        }
    }
}