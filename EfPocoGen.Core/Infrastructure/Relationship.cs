namespace EfPocoGen.Core.Infrastructure
{
    public enum Relationship
    {
        OneToOne,
        OneToMany,
        ManyToOne,
        ManyToMany,
        DoNotUse
    }
}