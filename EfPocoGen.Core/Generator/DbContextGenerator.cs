
namespace EfPocoGen.Core.Generator
{
    public class DbContextGenerator : AbstractGenerator
    {
        public override void Generate()
        {
            WriteUsing();
            WriteNamespace();
            WriteLine("public class {0} : DataContext", AppRefs.AppSetting.ContextName);
            Begin();

            //foreach (var tbl in from t in AppRefs.AllTables.Where(x => !x.IsMapping).OrderBy(x => x.NameHumanCase) select t)
            //{
            //    WriteLine("public {0} {1} {{ get; set; }}",
            //        AppRefs.GetInterfaceRepositoryClassName(tbl), Inflector.MakePlural(tbl.NameHumanCase));
            //}

            //WriteLine();
            WriteCtors();

            End();
        }

        public override string ClassName
        {
            get { return AppRefs.AppSetting.ContextName; }
        }

        public override string FullNamespace
        {
            get { return "Libs.DataAccess.Context"; }
        }

        private void WriteCtors()
        {
            WriteLine("static {0}()", AppRefs.AppSetting.ContextName);
            Begin();
            WriteLine("Database.SetInitializer<{0}>(null);", AppRefs.AppSetting.ContextName);
            End();
            WriteLine();

            WriteLine("public {0}(string nameOrConnectionString) : base(nameOrConnectionString)", AppRefs.AppSetting.ContextName);
            Begin();
            End();
        }

        private void WriteUsing()
        {
            WriteLine("using System;");
            WriteLine("using System.Data.Entity;");
            //WriteLine("using {0};", AppRefs.AppSetting.CoreContextNs);
            //WriteLine("using {0};", AppRefs.GetFullDataAccessNamespace(AppRefs.PocoNamespace));
            WriteLine();
        }
    }
}