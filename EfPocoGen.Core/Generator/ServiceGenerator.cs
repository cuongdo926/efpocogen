namespace EfPocoGen.Core.Generator
{
    public class ServiceGenerator : AbstractGenerator
    {
        public override string FullNamespace
        {
            get { return "Libs.Services"; }
        }

        private void WriteUsing()
        {
            WriteLine("using System;");
            WriteLine("using System.Collections.Generic;");
            WriteLine();
        }

        public override void Generate()
        {
            WriteUsing();
            WriteNamespace();
            WriteLine("public interface I{0}", ClassName);
            Begin();
            WriteLine("Task<{0}> Get(int id);", Table.ClassName);
            WriteLine("IQueryable<{0}Preview> GetList()", Table.ClassName);
            WriteLine("Task<{0}> Create({0}Model model);", Table.ClassName);
            WriteLine("Task<{0}> Update({0}Model model);", Table.ClassName);
            WriteLine("Task Delete(int id);");
            End();

            WriteLine();
            WriteLine("public class {0} : I{0}", ClassName);
            Begin();
            WriteLine("private readonly IEfUnitOfWork _unitOfWork;");
            WriteLine("private IRepository<{0}> {0}Repo => _unitOfWork.Repo<{0}>();", Table.ClassName);
            WriteLine();
            WriteLine("public {0}(IEfUnitOfWork unitOfWork)", Table.NameHumanCase);
            Begin();
            WriteLine("_unitOfWork = unitOfWork;");
            End();

            WriteLine();
            WriteLine("public Task<{0}> Get(int id)", Table.ClassName);
            Begin();
            WriteLine("return {0}Repo.GetAsync(id);", Table.ClassName);
            End();

            WriteLine();
            WriteLine("public IQueryable<{0}Preview> GetList()", Table.ClassName);
            Begin();
            WriteLine("var query = {0}Repo.AsNoTracking()", Table.ClassName);
            WriteLine("\t.ProjectTo<{0}Preview>();", Table.ClassName);
            WriteLine("return query;");
            End();

            WriteLine();
            WriteLine("public async Task<{0}> Create({0}Model model)", Table.ClassName);
            Begin();
            WriteLine("var entity = model.MapTo<{0}>();", Table.ClassName);
            WriteLine("{0}Repo.Insert(entity);", Table.ClassName);
            WriteLine("await _unitOfWork.SaveChangesAsync();");
            WriteLine("return entity;");
            End();

            WriteLine();
            WriteLine("public async Task<{0}> Update({0}Model model)", Table.ClassName);
            Begin();
            WriteLine("var entity = await {0}Repo.Queryable().Where(x => x.Id == model.Id).FirstOrDefaultAsync();", Table.ClassName);
            WriteLine("model.MapTo(entity);");
            WriteLine("await _unitOfWork.SaveChangesAsync();");
            WriteLine("return entity;");
            End();

            WriteLine();
            WriteLine("public async Task Delete(int id)");
            Begin();
            WriteLine("var entity = await {0}Repo.Queryable().Where(x => x.Id == id).FirstOrDefaultAsync();", Table.ClassName);
            WriteLine("PostRepo.Delete(entity);");
            WriteLine("await _unitOfWork.SaveChangesAsync();");
            End();

            End();
        }

        public override string ClassName => $"{Table.NameHumanCase}Service";
    }
}