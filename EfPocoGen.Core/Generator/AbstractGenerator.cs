using System.CodeDom.Compiler;
using System.IO;
using EfPocoGen.Core.Infrastructure;

namespace EfPocoGen.Core.Generator
{
    public abstract class AbstractGenerator : IndentedTextWriter
    {
        protected AbstractGenerator()
            : base(new StringWriter())
        {
        }

        public AppRef AppRefs { get; set; }
        public Table Table { get; set; }
        private bool _endNamespace;
        private bool _beginNamespace;

        protected void WriteNamespace()
        {
            if (_beginNamespace) return;
            _beginNamespace = true;
            WriteLine("namespace {0}", FullNamespace);
            Begin();
        }


        protected void Begin()
        {
            WriteLine("{");
            Indent++;
        }

        protected void End()
        {
            Indent--;
            WriteLine("}");
        }

        public abstract void Generate();
        public abstract string ClassName { get; }
        public abstract string FullNamespace { get; }

        public virtual string FileName => ClassName + ".cs";

        public override string ToString()
        {
            if (!_endNamespace)
            {
                _endNamespace = true;
                End();
            }
            return InnerWriter.ToString();
        }
    }
}