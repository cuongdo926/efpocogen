using System.Linq;

namespace EfPocoGen.Core.Generator
{
    public class ModelGenerator : AbstractGenerator
    {
        protected void WritePocoClassAttributes()
        {
            // Do nothing by default
            // Example:
            // if(t.ClassName.StartsWith("Order"))
            //     WriteLine("    [SomeAttribute]");
        }

        public override string FullNamespace
        {
            get { return "Libs.Models"; }
        }

        private void WriteUsing()
        {
            WriteLine("using System;");
            WriteLine("using {0};", AppRefs.AppSetting.CoreAutoMapperNs);
            WriteLine("using {0};", AppRefs.AppSetting.DomainEntityNs);
            WriteLine();
        }

        public override void Generate()
        {
            WriteUsing();
            WriteNamespace();
            WritePocoClassAttributes();
            WriteLine("public class {0} : IMapping<{1}>", ClassName, Table.ClassName);
            Begin();

            foreach (var col in Table.Columns.OrderBy(x => x.Ordinal).Where(x => !x.Hidden))
            {

                if (Table.IsUniqueIdEntity && col.IsPrimaryKey)
                {
                    WriteLine(col.EntityId);  // WritePocoColumn
                }
                else
                {
                    WriteLine(col.Entity);  // WritePocoColumn
                }
            }

            End();
        }

        public override string ClassName => $"{Table.ClassName}Model";
    }
}