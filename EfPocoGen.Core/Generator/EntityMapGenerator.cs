﻿using System.Linq;

namespace EfPocoGen.Core.Generator
{
    public class EntityMapGenerator : AbstractGenerator
    {
        private void WriteUsing()
        {
            WriteLine("using System.Data.Entity.ModelConfiguration;");
            WriteLine("using System.ComponentModel.DataAnnotations.Schema;");
            //WriteLine("using {0};", AppRefs.AppSetting.DomainEntityNs);
            WriteLine();
        }

        public override string FullNamespace
        {
            get { return "Libs.DataAccess.Configurations"; }
        }

        public override void Generate()
        {
            WriteUsing();
            WriteNamespace();
            WriteLine("internal class {0} : EntityTypeConfiguration<{1}>",
                ClassName, Table.NameHumanCase);
            Begin();
            //WriteLine("public {0}() : this(\"{1}\")",
            //   Table.EntityMapClassName,
            //   AppRefs.SchemaName);
            //Begin();
            //End();
            //WriteLine();
            WriteLine("public {0}()", ClassName);
            Begin();
            WriteLine("ToTable(\"{0}.{1}\");", AppRefs.SchemaName, Table.Name);
            if (Table.IsUniqueIdEntity)
            {
                WriteLine("HasKey(x => x.Id);");
                //WriteLine("HasKey({0});", Table.PrimaryKeyNameHumanCase());
            }
            WriteLine();
            foreach (var col in Table.Columns.Where(x => !x.Hidden).OrderBy(x => x.Ordinal))
            {
                if (Table.IsUniqueIdEntity && col.IsPrimaryKey)
                {
                    Write("Property(x => x.Id).HasColumnName(\"{0}\").IsRequired()", col.Name);
                    if (col.IsIdentity)
                    {
                        Write(".HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)");
                    }
                    WriteLine(";");
                }
                else
                {
                    WriteLine(col.Config);
                }
            }

            if (Table.HasForeignKey)
            {
                WriteLine();
                foreach (var col in from c in Table.Columns.OrderBy(x => x.Ordinal) where c.ConfigFk != null select c)
                {
                    WriteLine(col.ConfigFk);
                }
            }

            // many to many
            foreach (var s in Table.MappingConfiguration)
            {
                WriteLine(s);
            }

            End();
            End();
        }

        public override string ClassName => $"{Table.ClassName}Configuration";
    }
}
