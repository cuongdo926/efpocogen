using System.Linq;
using EfPocoGen.Core.Infrastructure;

namespace EfPocoGen.Core.Generator
{
    public class EntityGenerator : AbstractGenerator
    {
        public override string FullNamespace
        {
            get { return "Libs.DataAccess.Entities"; }
        }

        // Writes optional base classes
        protected string WritePocoBaseClasses()
        {
            if (Table.IsUniqueIdEntity)
            {
                var type = Table.PrimaryKeys.First().PropertyType;
                switch (Table.AuditType)
                {
                    case AuditType.Creation:
                        return $"CreationAuditedEntity<{type}>";
                    case AuditType.Modification:
                        return $"ModificationAuditedEntity<{type}>";
                }
            }
            return "EntityBase";
        }

        private void WriteUsing()
        {
            WriteLine("using System;");
            WriteLine("using System.Collections.Generic;");
            //WriteLine("using {0};", AppRefs.AppSetting.CoreEntityNs);
            WriteLine();
        }

        public override void Generate()
        {
            WriteUsing();
            WriteNamespace();
            WriteLine("public class {0} : {1}", ClassName, WritePocoBaseClasses());
            Begin();

            foreach (var col in Table.Columns.OrderBy(x => x.Ordinal).Where(x => !x.Hidden))
            {
                if (col.IsRowVersion || col.ColumnType == ColumnType.Audit) continue;

                if (Table.IsUniqueIdEntity && col.IsPrimaryKey) continue;

                WriteLine(col.Entity);
            }

            WriteLine();
            foreach (var s in Table.ReverseNavigationProperty.OrderBy(x => x))
            {
                WriteLine(s);
            }

            if (Table.HasForeignKey)
            {
                foreach (var col in from c in Table.Columns.OrderBy(x => x.EntityFk) where c.EntityFk != null select c)
                {
                    if (col.ColumnType == ColumnType.Audit)
                    {
                        WriteLine($"[ForeignKey(\"{col.Name}\")]");
                        WriteLine(col.EntityFk);
                    }
                    else
                    {
                        WriteLine(col.EntityFk);
                    }
                }
            }

            if (Table.Columns.Any(c => c.Default != string.Empty) ||
                Table.ReverseNavigationCtor.Any())
            {
                WriteLine();
                WriteLine("public {0}()", Table.NameHumanCase);
                Begin();
                foreach (var col in Table.Columns.OrderBy(x => x.Ordinal).Where(c => c.Default != string.Empty))
                {
                    WriteLine("{0} = {1};", col.PropertyNameHumanCase, col.Default);
                }

                foreach (var s in Table.ReverseNavigationCtor)
                {
                    WriteLine(s);
                }
                End();
            }
            End();
        }

        public override string ClassName => Table.NameHumanCase;
    }
}